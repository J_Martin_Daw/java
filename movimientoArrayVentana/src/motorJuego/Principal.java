package motorJuego;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import clases.MotorJuego;
import ventana.VentanaJuego;

public class Principal {
	
	static char caracter;

	public static void main(String[] args) {
		int filas = 11;
		int columnas = 11;
		VentanaJuego ventana = new VentanaJuego(filas, columnas);
		ventana.setVisible(true);
		ventana.getTextArea().addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				caracter=e.getKeyChar();
			}
		});
		MotorJuego tablero = new MotorJuego(filas, columnas);
		
		int contadorMovimientos = 0;

		do {
			try {
				Thread.sleep(300);
			} catch (InterruptedException excepcion) {
				excepcion.printStackTrace();
			}
			contadorMovimientos++;
			tablero.turno(caracter);
			caracter='n';
			
			tablero.mostrarEspacioVentana(ventana.getTextArea());
			
		} while (contadorMovimientos < 50);
	}



}
