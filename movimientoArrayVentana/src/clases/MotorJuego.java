package clases;

import javax.swing.JTextArea;

public class MotorJuego {
	private final static char ICONO_JUGADOR = 'W'; 
	private final static char ESPACIO_VACIO = ' '; 
	private final static String SALTO_DE_LINEA = "\n"; 
	private int posicionVertical;
	private int posicionHorizontal;
	private char[][] arrayJuego;
	
	public MotorJuego(int numCasillasVertical, int numCasillasHorizontal) {
		
		this.arrayJuego = new char[numCasillasVertical][numCasillasHorizontal];
		posicionVertical = numCasillasVertical-1;
		posicionHorizontal = numCasillasHorizontal/2; 
		/*el icono del jugador empieza en la mitad inferior del tablero*/
		rellenarTablero();
		
	}
	
	public void rellenarTablero() {
		generarCasillasJugador();
		generarCasillasVacias();
	}
	
	public void generarCasillasJugador(){
		arrayJuego[posicionVertical][posicionHorizontal] = ICONO_JUGADOR;
	}
	
	public void generarCasillasVacias(){
		for (int i = 0; i < arrayJuego.length; i++) {
			for (int j = 0; j < arrayJuego[i].length; j++) {
				if (arrayJuego[i][j] != ICONO_JUGADOR) {
					arrayJuego[i][j] = ESPACIO_VACIO;
				}
			}
		}
	}

	
	public void turno(char tecla){

		movimientoValido(tecla);
		
	}

	public void movimientoValido(char tecla) {
		switch (tecla) {
		/*cada caso comprueba que el movimiento no sale del espacio de juego*/
		case 'w':
			if (posicionVertical-1 < 0 == false) {
				movimientoJugador(tecla);
			}
			break;
		case 'a':
			if (posicionHorizontal-1 < 0 == false) {
				movimientoJugador(tecla);
			}
			break;
		case 's':
			if (posicionVertical+1 > arrayJuego.length-1 == false) {
				movimientoJugador(tecla);
			}
			break;
		case 'd':
			if (posicionHorizontal+1 > arrayJuego[0].length-1 == false) {
				movimientoJugador(tecla);
			}
			break;
		default:
			break;
		}
	}
	
	public void movimientoJugador( char tecla){
		switch (tecla) {
		case 'w':
			arrayJuego[posicionVertical-1][posicionHorizontal]=ICONO_JUGADOR;
			espacioVacio();
			posicionVertical--;
			break;
		case 'a':
			arrayJuego[posicionVertical][posicionHorizontal-1]=ICONO_JUGADOR;
			espacioVacio();
			posicionHorizontal--;
			break;
		case 's':
			arrayJuego[posicionVertical+1][posicionHorizontal]=ICONO_JUGADOR;
			espacioVacio();
			posicionVertical++;
			break;

		case 'd':
			arrayJuego[posicionVertical][posicionHorizontal+1]=ICONO_JUGADOR;
			espacioVacio();
			posicionHorizontal++;
			break;
		}
	}
	public void espacioVacio() {
		arrayJuego[posicionVertical][posicionHorizontal]=ESPACIO_VACIO;
	}
	
	public void mostrarEspacioVentana(JTextArea areaTexto){
		
		areaTexto.setText(" ");
		StringBuilder cadena = new StringBuilder();

		for (int i = 0; i < arrayJuego.length; i++) {
			for (int j = 0; j < arrayJuego[i].length; j++) {
			
					cadena.append(arrayJuego[i][j]+"  ");
					
			}
			cadena.append(SALTO_DE_LINEA);
		}
		areaTexto.setText(cadena.toString());
	}
	

}
