package ventana;


import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.BorderLayout;


public class VentanaJuego extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextArea textArea;
	private int anchura;
	private int altura;

	/**
	 * Crea la ventana.
	 */
	public VentanaJuego(int filas, int columnas) {
		setResizable(false);
		anchura = columnas*24;
		altura = filas*21;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		
		setBounds(100, 100, anchura, altura);
		contentPane = new JPanel();
		
		setContentPane(contentPane);
		
		textArea = new JTextArea(contentPane.getHeight(), contentPane.getWidth());
		textArea.setFont(new Font("Monospaced", Font.PLAIN, 13));
		textArea.setEditable(false);
		
		Dimension dimensionVentana = new Dimension(anchura, altura);
		contentPane.setLayout(new BorderLayout(0, 0));
		textArea.setPreferredSize(dimensionVentana );
		textArea.setMaximumSize(dimensionVentana);
		textArea.setMinimumSize(dimensionVentana);
		contentPane.add(textArea);

	}

	public JTextArea getTextArea() {
		return textArea;
	}
}
