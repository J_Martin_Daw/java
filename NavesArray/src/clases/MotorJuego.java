package clases;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import ventana.VentanaJuego;

public class MotorJuego {
	private static final char RESETEAR_CARACTER = 'm';
	private static final char ICONO_JUGADOR = 'W'; 
	private static final char ICONO_EXPLOSION = '*'; 
	private static final char PROYECTIL_ENEMIGO = 'V'; 
	private static final char ESPACIO_VACIO = ' ';
	private static final char PROYECTIL_ALIADO = '^';
	private static final int MAX_ENFRIAMIENTO = 4;
	
	private int posicionVertical;
	private int posicionHorizontal;
	private int enfriamiento;
	private int contadorTurnos;
	private char[][] arrayJuego;
	private VentanaJuego ventana;
	private boolean turnoProyectilesEnemigos;
	private boolean turnoDisparo;
	private boolean perder;
	
	private char teclaPulsada;

	static long puntuacion;

	public MotorJuego(int numCasillasVertical, int numCasillasHorizontal) {
		
		arrayJuego = new char[numCasillasVertical][numCasillasHorizontal];
		/*el icono del jugador empieza en la mitad inferior del tablero*/
		posicionVertical = numCasillasVertical-2;
		posicionHorizontal = numCasillasHorizontal/2; 
		darValorVariables();
		rellenarTablero();
		
	}
	
	public void generarVentana(int filas, int columnas){
		
		ventana = new VentanaJuego(filas, columnas);
		ventana.setVisible(true);
		ventana.getTextArea().addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				String aux=String.valueOf(e.getKeyChar());
				teclaPulsada = aux.toLowerCase().charAt(0);
			}
		});
	}
	
	private void darValorVariables() {
		
		turnoProyectilesEnemigos = false;
		perder = false;
		contadorTurnos = 0;
		puntuacion = 0;
		enfriamiento = 0;
		
	}

	public void rellenarTablero() {
		generarCasillasJugador();
		generarCasillasVacias();
	}
	
	public void generarCasillasJugador(){
		arrayJuego[posicionVertical][posicionHorizontal] = ICONO_JUGADOR;
	}
	
	public void generarCasillasVacias(){
		for (int i = 0; i < arrayJuego.length; i++) {
			for (int j = 0; j < arrayJuego[i].length; j++) {
				if (arrayJuego[i][j] != ICONO_JUGADOR) {
					arrayJuego[i][j] = ESPACIO_VACIO;
				}
			}
		}
	}

	public void turno(){
		movimientoJugador();
		generarProyectilEnemigo();
		accionTablero();
		terminarTurno();
		contadorTurnos++;	
	}

	private void movimientoJugador() {
		switch (teclaPulsada) {
		/*cada caso comprueba que el movimiento no sale del espacio de juego*/
		case 'w':
			if (enfriamiento == 0) {
				accionJugador();
			}
			break;
		case 'a':
			if (posicionHorizontal-1 < 0 == false) {
				accionJugador();
			}
			break;
		case 'd':
			if (posicionHorizontal+1 > arrayJuego[0].length-1 == false) {
				accionJugador();
			}
			break;
		default:
			break;
		}
	}

	public void generarProyectilEnemigo() { 
		int aleatorio = generarCasillaAleatoria();
		switch (contadorTurnos) {
		case 6:
			turnoProyectilesEnemigos = true; 
			puntuacion += 5;
			contadorTurnos = 0;
			if (aleatorio-2 >= 0) {
				arrayJuego[0][aleatorio-2] = PROYECTIL_ENEMIGO;
			}	
			if (aleatorio+2 < arrayJuego[0].length) {
				arrayJuego[0][aleatorio+2] = PROYECTIL_ENEMIGO;
			}
			
		case 3:
			turnoProyectilesEnemigos = true; 
			if (aleatorio-1 >= 0) {
				arrayJuego[0][aleatorio-1] = PROYECTIL_ENEMIGO;
			}
			if (aleatorio+1 < arrayJuego[0].length) {
				arrayJuego[0][aleatorio+1] = PROYECTIL_ENEMIGO;
			}
			
		case 0:
			turnoProyectilesEnemigos = true; 
			arrayJuego[0][aleatorio] = PROYECTIL_ENEMIGO;
			break;
		}
	}

	public void accionJugador() {
		switch (teclaPulsada) {
		case 'w':
			generarProyectilAliado();
			enfriamiento = MAX_ENFRIAMIENTO;
			break;
		case 'a':
			arrayJuego[posicionVertical][posicionHorizontal-1]=ICONO_JUGADOR;
			arrayJuego[posicionVertical][posicionHorizontal]=ESPACIO_VACIO;
			posicionHorizontal--;
			break;
		/*case 's':
			arrayJuego[posicionVertical+1][posicionHorizontal]=ICONO_JUGADOR;
			espacioVacio();
			posicionVertical++;
			break;*/

		case 'd':
			arrayJuego[posicionVertical][posicionHorizontal+1]=ICONO_JUGADOR;
			arrayJuego[posicionVertical][posicionHorizontal]=ESPACIO_VACIO;
			posicionHorizontal++;
			break;
		}
	}
	
	public void generarProyectilAliado() {
		turnoDisparo = true;
		enfriamiento = MAX_ENFRIAMIENTO;
		arrayJuego[posicionVertical-1][posicionHorizontal] = PROYECTIL_ALIADO;
		
	}
	


	private int generarCasillaAleatoria() {
		return (int)(Math.random()*arrayJuego[0].length);
		
	}

	public void accionTablero() {
		comprobarProyectilesEnemigos();
		comprobarProyectilesAliados();
		ventana.mostrarEspacioVentana(arrayJuego, puntuacion); 
		
	}

	private void comprobarProyectilesEnemigos() {
		/*se recorre el array desde abajo a la derecha hasta arriba a la izquierda debido a la 
		 * direccion en la que van los proyectiles enemigos*/
		for (int i =  arrayJuego.length-1; i >= 0; i--) {/*vertical*/
			for (int j =  arrayJuego[i].length-1; j >= 0; j--) {/*horizontal*/
				
				if (arrayJuego[i][j] == PROYECTIL_ENEMIGO) {
					if ((i > 0) || (i == 0 && !turnoProyectilesEnemigos)/*rev*/) {
						if (!(i==arrayJuego.length-1)) {
							if(arrayJuego[i+1][j] == ICONO_JUGADOR) {
								arrayJuego[i+1][j] = ICONO_EXPLOSION;
								perder = true;
							}else if(arrayJuego[i+1][j] == PROYECTIL_ALIADO){
								arrayJuego[i+1][j] = ICONO_EXPLOSION;
							}else {
								arrayJuego[i+1][j] = PROYECTIL_ENEMIGO;
							}
							
						}
						arrayJuego[i][j] = ESPACIO_VACIO;
					}
				}
			}
		}
		turnoProyectilesEnemigos = false;

	}
	private void comprobarProyectilesAliados() {
		for (int i = 0; i < arrayJuego.length; i++) {
			for (int j = 0; j < arrayJuego[i].length; j++) {
				
				if (arrayJuego[i][j] == PROYECTIL_ALIADO) {
					if ((i < posicionVertical-1) || (i == posicionVertical-1 && !turnoDisparo)) {
						if (!(i==0)) {
							if(arrayJuego[i-1][j] == PROYECTIL_ENEMIGO){
								arrayJuego[i-1][j] = ICONO_EXPLOSION;
							}else {
								arrayJuego[i-1][j] = PROYECTIL_ALIADO;
							}
							
						}
						arrayJuego[i][j] = ESPACIO_VACIO;
					}
				}
				
			}
		}
		
	}

	public void terminarTurno() {
		limpiarExplosiones();
		gestionarEnfriamiento();
		teclaPulsada = RESETEAR_CARACTER;
		if (perder) {
			ventana.mostrarCartelFinal(puntuacion);
		}
		
	}

	private void limpiarExplosiones() {
		for (int i = 0; i < arrayJuego.length; i++) {
			for (int j = 0; j < arrayJuego[i].length; j++) {
				
				if (arrayJuego[i][j] == ICONO_EXPLOSION) {
					arrayJuego[i][j] = ESPACIO_VACIO;
				}
			}
		}
	}
	
	public void gestionarEnfriamiento() {
		if (turnoDisparo) {
			turnoDisparo = false;
		}else {
			switch (enfriamiento) {
			case 0:
				break;
			default:
				enfriamiento--;
				break;
			}
		}
		
	}
	
	public VentanaJuego getVentana() {
		return ventana;
	}
	
	public boolean getPerder() {
		return perder;
	}
}
