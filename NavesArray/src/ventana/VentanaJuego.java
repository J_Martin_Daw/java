package ventana;


import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import java.awt.Font;
import java.awt.BorderLayout;
import java.awt.Color;


public class VentanaJuego extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String TITULO = "Naves";
	private static final String SALTO_LINEA = "\n";
	private static final int RESCALAR_HORIZONTAL = 25;
	private static final int RESCALAR_VERTICAL = 23;
	private JPanel contentPane;
	private JTextArea textArea;
	private int anchura;
	private int altura;

	/**
	 * Crea la ventana.
	 */
	public VentanaJuego(int filas, int columnas) {
		super(TITULO);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		anchura = columnas*RESCALAR_HORIZONTAL;
		altura = filas*RESCALAR_VERTICAL;
		setBounds(100, 100, anchura, altura);
		contentPane = new JPanel();
		
		setContentPane(contentPane);
		
		textArea = new JTextArea(filas, columnas);
		textArea.setFont(new Font("Monospaced", Font.PLAIN, 14));
		textArea.setEditable(false);
		textArea.setBackground(Color.black);
		textArea.setForeground(Color.white);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		contentPane.add(textArea);

	}
	
	public void mostrarEspacioVentana(char[][] arrayJuego, long puntuacion) {	
		StringBuilder cadena = new StringBuilder();
		for (int i = 0; i < textArea.getRows(); i++) {
			for (int j = 0; j < textArea.getColumns(); j++) {	
					cadena.append(arrayJuego[i][j]+"  ");			
			}
			cadena.append(SALTO_LINEA);
		}
		cadena.append("Puntuacion: "+puntuacion);
		textArea.setText(cadena.toString());
	}
	
	public void mostrarCartelFinal(long puntuacion) {
		StringBuilder cadena = new StringBuilder();
		for (int i = 0; i < (textArea.getRows()/2); i++) {
			cadena.append(SALTO_LINEA);
		}
		for (int i = 0; i < (textArea.getColumns()/2); i++) {
			cadena.append("  ");
		}cadena.append("!!!HAS PERDIDO!!!"+SALTO_LINEA);
		for (int i = 0; i < (textArea.getColumns()/2); i++) {
			cadena.append("  ");
		}cadena.append("Puntuacion: " + puntuacion);
		textArea.setText(cadena.toString());
	}
	
	public JTextArea getTextArea() {
		return textArea;
	}
	
	public void pausarAplicacion(int segundos) {
		try {
			Thread.sleep(segundos);
		} catch (InterruptedException excepcion) {
			excepcion.printStackTrace();
		}
		
	}

}