package main;

import clases.MotorJuego;

public class Principal {
	
	
	static MotorJuego tablero;
	
	final static int TIEMPO_PAUSA = 130;/*tiempo que dura cada frame del juego en milisegundos*/
	final static int FILAS = 25;
	final static int COLUMNAS = 15;

	public static void main(String[] args) {
		
		tablero = new MotorJuego(FILAS, COLUMNAS);
		tablero.generarVentana(FILAS, COLUMNAS);
		do {
			tablero.turno();
			tablero.getVentana().pausarAplicacion(TIEMPO_PAUSA);			
		} while (!tablero.getPerder());
	}

}
