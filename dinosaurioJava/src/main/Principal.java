package main;

import clases.MotorJuego;

public class Principal {
	
	
	static MotorJuego tablero;
	
	final static int TIEMPO_PAUSA = 300;/*tiempo que dura cada frame del juego en milisegundos*/
	final static int FILAS = 10;
	final static int COLUMNAS = 15;

	public static void main(String[] args) {
		
		tablero = new MotorJuego(FILAS, COLUMNAS);
		do {
			tablero.turno();
			tablero.getVentana().pausarAplicacion(TIEMPO_PAUSA);			
		} while (!tablero.getPerder());
	}

}
