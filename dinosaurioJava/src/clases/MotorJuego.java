package clases;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import ventana.VentanaJuego;

public class MotorJuego {
	private static final char ICONO_JUGADOR = 'X'; 
	private static final char ESPACIO_VACIO = '-';
	private static final int POSICION_INICIAL_HORIZONTAL = 4;
	private static final int DURACION_SALTO = 4;
	
	private int posicionVertical;
	private int posicionHorizontal;
	private char[][] arrayJuego;
	private VentanaJuego ventana;
	private boolean saltoDisponible;
	private boolean perder;
	
	private int saltoActual;

	static long puntuacion;

	public MotorJuego(int numCasillasVertical, int numCasillasHorizontal) {
		
		arrayJuego = new char[numCasillasVertical][numCasillasHorizontal];
		posicionVertical = 0;
		posicionHorizontal = POSICION_INICIAL_HORIZONTAL; 
		darValorVariables();
		rellenarTablero();
		generarVentana(numCasillasVertical, numCasillasHorizontal);
		
	}
	
	public void generarVentana(int filas, int columnas){
		
		ventana = new VentanaJuego(filas, columnas);
		ventana.setVisible(true);
		ventana.getTextArea().addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				char aux = e.getKeyChar();
				if (aux == ' ') {
					intentarSaltar();
				}
			}
		});
	}
	
	private void darValorVariables() {
		
		perder = false;
		puntuacion = 0;
		saltoDisponible = true;;
		
	}
	
	public void rellenarTablero() {

		arrayJuego[posicionVertical][posicionHorizontal] = ICONO_JUGADOR;

		for (int i = 0; i < arrayJuego.length; i++) {
			for (int j = 0; j < arrayJuego[i].length; j++) {
				if (arrayJuego[i][j] != ICONO_JUGADOR) {
					arrayJuego[i][j] = ESPACIO_VACIO;
				}
			}
		}
	}
	
	private void intentarSaltar() {
		if (saltoDisponible) {
			saltoActual += DURACION_SALTO ;
		}
		
	}

	
	public void turno(){
		accionTablero();
		terminarTurno();
	}

	/*private int generarCasillaAleatoria() {
		return (int)(Math.random()*arrayJuego[0].length);
		
	}*/

	public void accionTablero() {
		/*generarPlataforma();*//*generara plataformas horizontales sobre las que el juegador saltara para no caer*/
		fisicasPersonaje();
		ventana.mostrarEspacioVentana(arrayJuego, puntuacion); 
		
	}

	/*private void generarPlataforma() {
		/*se recorre el array desde abajo a la derecha hasta arriba a la izquierda debido a la 
		 * direccion en la que van los proyectiles enemigos*/
		/*for (int i =  arrayJuego.length-1; i >= 0; i--) {/*vertical*/
			/*for (int j =  arrayJuego[i].length-1; j >= 0; j--) {/*horizontal*/
				
				/*if (arrayJuego[i][j] == PROYECTIL_ENEMIGO) {
					if ((i > 0) || (i == 0 && !turnoProyectilesEnemigos)) {
						if (!(i==arrayJuego.length-1)) {
							if(arrayJuego[i+1][j] == ICONO_JUGADOR) {
								arrayJuego[i+1][j] = ICONO_EXPLOSION;
								perder = true;
							}else if(arrayJuego[i+1][j] == PROYECTIL_ALIADO){
								arrayJuego[i+1][j] = ICONO_EXPLOSION;
							}else {
								arrayJuego[i+1][j] = PROYECTIL_ENEMIGO;
							}
							
						}
						arrayJuego[i][j] = ESPACIO_VACIO;
					}
				}
			}
		}

	}*/

	private void fisicasPersonaje() {
		
		if (saltoActual > 0 && posicionVertical-1 >= 0) {
			arrayJuego[posicionVertical-1][posicionHorizontal] = ICONO_JUGADOR;
			arrayJuego[posicionVertical][posicionHorizontal] = ESPACIO_VACIO;
			posicionVertical--;
			saltoActual--;
		}else if (posicionVertical+1 < arrayJuego.length && cayendo()) {
			arrayJuego[posicionVertical+1][posicionHorizontal] = ICONO_JUGADOR;
			arrayJuego[posicionVertical][posicionHorizontal] = ESPACIO_VACIO;
			posicionVertical++;
		}
		
	}

	private boolean cayendo() {
		return arrayJuego[posicionVertical+1][posicionHorizontal] == ESPACIO_VACIO;
	}

	public void terminarTurno() {
		if (posicionVertical == arrayJuego.length-1 || (posicionVertical+1 < arrayJuego.length && arrayJuego[posicionVertical+1][posicionHorizontal] != ESPACIO_VACIO)) {
			/*^^^^codigo provisional, al caer al suelo mueres*/
			/*en el caso de que este apoyado en una plataforma*/
			saltoDisponible = true;
		}else {
			saltoDisponible = false;
		}
		if (perder) {
			ventana.mostrarCartelFinal(puntuacion);
		}
		
	}
	
	public VentanaJuego getVentana() {
		return ventana;
	}
	
	public boolean getPerder() {
		return perder;
	}
}
